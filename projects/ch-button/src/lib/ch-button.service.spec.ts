import { TestBed } from '@angular/core/testing';

import { ChButtonService } from './ch-button.service';

describe('ChButtonService', () => {
  let service: ChButtonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChButtonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
