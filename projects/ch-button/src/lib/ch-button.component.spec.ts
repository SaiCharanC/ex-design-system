import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChButtonComponent } from './ch-button.component';

describe('ChButtonComponent', () => {
  let component: ChButtonComponent;
  let fixture: ComponentFixture<ChButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
