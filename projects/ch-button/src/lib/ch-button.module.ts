import { NgModule } from '@angular/core';
import { ChButtonComponent } from './ch-button.component';



@NgModule({
  declarations: [ChButtonComponent],
  imports: [
  ],
  exports: [ChButtonComponent]
})
export class ChButtonModule { }
