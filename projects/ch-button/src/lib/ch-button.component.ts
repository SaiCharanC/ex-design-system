import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ch-button',
  template: `
    <p>
      ch-button works!
    </p>
  `,
  styles: [
  ]
})
export class ChButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
